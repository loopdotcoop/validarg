# validarg

__Succinctly validates JavaScript values, especially arguments.__

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/loopdotcoop/validarg/>  
▶&nbsp; __Homepage:__ <https://loopdotcoop.gitlab.io/validarg/>  


Typical usage:

```js
function myFunction(arg1, isProduction) {
    const v = new Validarg('myFunction()', isProduction);
    if (!v.boolean(arg1, 'arg1')) return v.err;
    return 'ok!';
}
myFunction(true); // ok!
myFunction(123); // myFunction(): 'arg1' is type 'number' not 'boolean'
myFunction(123, true); // ok! (for a less safe, but faster production app)
```


## Check Global Dependencies

1.  Check your __git__ version:  
    `git --version # should be 'git version 2.30.0'`
2.  Check your __node__ version:  
    `node --version # should be 'v16.13.0'`
3.  Check your __npm__ version:  
    `npm --version # should be '8.1.0'`
4.  Check your __rollup__ version:  
    `rollup --version # should be 'rollup v2.75.7'`
5.  Check your __static-server__ version:  
    `static-server --version # should be 'static-server@2.2.1'`


## Handy Dev Commands

Run the ‘src/’ test suite, during development:  
`static-server & open http://localhost:9080/ && fg # test src/ using a browser`  
`node src/node-test.js # test src/ using NodeJS`

Build the minified and unminified bundles, using settings in rollup.config.js:  
`rollup -c`  
or  
`npm run build`

Run the ‘public/’ test suite, after a build:  
`static-server public & open http://localhost:9080/ && fg # test public/ using a browser`  
`npm test # test public/ using NodeJS`  
