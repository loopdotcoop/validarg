/* -------------------------------- Constants ------------------------------- */

const BOLD = '\u001b[1m';
const DLOB = '\u001b[0m';
const DIM  = '\u001b[2m';
const MID  = '\u001b[0m';
const PASS = '\u001b[32m√ ';
const SSAP = '\u001b[0m';
const FAIL = '\u001b[31mX ';
const LIAF = '\u001b[0m';

const out = [];


/* ------------------------------ Main Function ----------------------------- */

// @TODO describe
export default function expect(title, result) {
    return {
        // Colour the page red if a line begins ‘Failed ’.
        // Otherwise colour the page green.
        done() {
            // document.body.classList.add($pre.innerText.split('\n').reduce((acc, line) =>
            // acc==='fail'||line.slice(0, 7)==='Failed '?'fail':'pass', 0));
            return out.join('\n');
        },
        section(title) {
            out.push(`${BOLD}${title}:${DLOB}`);
        },
        toBe(expected) {
            if (result !== expected)
                out.push(
                    `${FAIL}Failed${LIAF} ${title}:`,
                    `  ${DIM}expected:${MID} ${expected}`,
                    `  ${DIM}actually:${MID} ${result}`
                );
            else
                out.push(`${PASS}Passed${SSAP} ${title}`);
        },
        toError(expected) {
            if (result?.error !== expected)
                out.push(
                    `${FAIL}Failed${LIAF} ${title}:`,
                    `  ${DIM}expected:${MID} ${expected}`,
                    `  ${DIM}actually:${MID} ${result?.error}`
                );
            else
                out.push(`${PASS}Passed${SSAP} ${title}`);
        },
        toJson(expected) {
            const rJson = JSON.stringify(result);
            const eJson = JSON.stringify(expected);
            if (rJson !== eJson)
                return out.push(
                    `${FAIL}Failed${LIAF} ${title}:`,
                    `  ${DIM}expected:${MID} ${eJson}`,
                    `  ${DIM}actually:${MID} ${rJson}`
                );
            else
                out.push(`${PASS}Passed${SSAP} ${title}`);
        },
        toHave(expected) {
            if (result.error)
                return out.push(
                    `${FAIL}Failed${LIAF} ${title}:`,
                    `  result is an error: ${result.error}`
                );
            for (let key in expected) {
                const rJson = JSON.stringify(result[key]);
                const eJson = JSON.stringify(expected[key]);
                if (rJson !== eJson)
                    return out.push(
                        `${FAIL}Failed${LIAF} ${title}:`,
                        `  ${key} is ${rJson} not ${eJson}`,
                    );
            }
            out.push(`${PASS}Passed${SSAP} ${title}`);
        },
        toMatch(expected) {
            if (! expected.test(result))
                out.push(
                    `${FAIL}Failed${LIAF} ${title}:`,
                    `  ${DIM}expected:${MID} ${expected}`,
                    `  ${DIM}actually:${MID} ${result}`
                );
            else
                out.push(`${PASS}Passed${SSAP} ${title}`);
        },
    }
}
