// validarg/public/node-test.js

/* -------------------------------- Imports --------------------------------- */

import expect from '../lib/expect.js';
import Validarg from './lib/validarg.es.js';
import validargTest from './lib/validarg-test.es.js';


/* --------------------------------- Tests ---------------------------------- */

validargTest(expect, Validarg);
console.log(expect().done());
