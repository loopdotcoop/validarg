// Configuration, used by `rollup -c` during `npm run build`.

import { babel } from '@rollup/plugin-babel';
import { terser } from 'rollup-plugin-terser';

export default [

    // Bundle the `Validarg` class. Tree-shaking should remove all tests.
    {
        input: 'src/validarg-main.js',
        output: {
            file: 'public/lib/validarg.es.js',
            format: 'es', // eg for `node public/node-test.js`
        }
    },
    {
        input: 'src/validarg-main.js',
        output: {
            file: 'public/lib/validarg.umd.es5.min.js',
            format: 'umd', // eg for `public/index.html` in legacy browsers
            name: 'validarg.main', // `const Validarg = validarg.main`
        },
        // See https://babeljs.io/docs/en/babel-preset-env
        // and https://github.com/terser/terser#minify-options
        plugins: [
            babel({ babelHelpers: 'bundled' }),
            terser({ keep_classnames:true })
        ]
    },

    // Bundle the tests. Tree-shaking should remove the `Validarg` class.
    {
        input: 'src/validarg-test.js',
        output: {
            file: 'public/lib/validarg-test.es.js',
            format: 'es', // eg for `node public/node-test.js`
        }
    },
    {
        input: 'src/validarg-test.js',
        output: {
            file: 'public/lib/validarg-test.umd.es5.js',
            format: 'umd', // eg for `public/index.html` in legacy browsers
            name: 'validarg.test' // `validarg.test(expect, Validarg)`
        },
        plugins: [ babel({ babelHelpers: 'bundled' }) ]
    }

];
