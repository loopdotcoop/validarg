// validarg/src/node-test.js

/* -------------------------------- Imports --------------------------------- */

import expect from '../lib/expect.js';
import Validarg from './validarg-main.js';
import validargTest from './validarg-test.js';


/* --------------------------------- Tests ---------------------------------- */

validargTest(expect, Validarg);
console.log(expect().done());
