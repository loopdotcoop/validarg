// validarg/src/validarg-main.js

// The ‘main’ file for bundling the `Validarg` class.
// Tree-shaking should remove all tests.

import Validarg from './validarg.js';
export default Validarg;
