// validarg/src/validarg-test.js

// The ‘main’ file for bundling Validarg unit tests.
// Tree-shaking should remove the `Validarg` class.

import { test as testValidarg } from './validarg.js';

import { test as testArray } from './methods/array.js';
import { test as testBoolean } from './methods/boolean.js';
import { test as testInteger } from './methods/integer.js';
import { test as testNumber } from './methods/number.js';
import { test as testObject } from './methods/object.js';
import { test as testSchema } from './methods/schema.js';
import { test as testString } from './methods/string.js';

// Run each test. You can comment-out some during development, to help focus on
// individual tests. But make sure all tests are uncommented before committing.
export default function validargTest(expect, Validarg) {

    testValidarg(expect, Validarg);

    testArray(expect, Validarg);
    testBoolean(expect, Validarg);
    testInteger(expect, Validarg);
    testNumber(expect, Validarg);
    testObject(expect, Validarg);
    testSchema(expect, Validarg);
    testString(expect, Validarg);

}
