// validarg/src/validarg.js

// Assembles the `Validarg` class.


/* --------------------------------- Import --------------------------------- */

import _type from './methods/_type.js';
import _validateAgainstSchema from './methods/_validate-against-schema.js';

import array from './methods/array.js';
import boolean from './methods/boolean.js';
import integer from './methods/integer.js';
import number from './methods/number.js';
import object from './methods/object.js';
import schema from './methods/schema.js';
import string from './methods/string.js';


/* ---------------------------------- Class --------------------------------- */

// Succinctly validates JavaScript values, especially arguments.
// Typical usage:
//     function myFunction(arg1, isProduction) {
//         const v = new Validarg('myFunction()', isProduction);
//         if (!v.boolean(arg1, 'arg1')) return v.err;
//         return 'ok!';
//     }
//     myFunction(true); // ok!
//     myFunction(123); // myFunction(): 'arg1' is type 'number' not 'boolean'
//     myFunction(123, true); // ok! (for a less safe, but faster production app)
export default class Validarg {

    constructor (prefix, skip) {
        this.err = null;
        this.prefix = prefix || '(anon)';
        this.skip = skip || false;
    }

}

Validarg.prototype._type = _type;
Validarg.prototype._validateAgainstSchema = _validateAgainstSchema;

Validarg.prototype.array = array;
Validarg.prototype.boolean = boolean;
Validarg.prototype.integer = integer;
Validarg.prototype.number = number;
Validarg.prototype.object = object;
Validarg.prototype.schema = schema;
Validarg.prototype.string = string;


/* ---------------------------------- Test ---------------------------------- */

// Runs basic tests on Validarg.
export function test(expect, Validarg) {
    expect().section('Validarg basics');
    expect(`typeof Validarg`, typeof Validarg).toBe('function');
    expect(`new Validarg()`,
            new Validarg()).toHave({
                err:null, prefix:'(anon)', skip:false });
    expect(`new Validarg('foo()', true)`,
            new Validarg('foo()', true)).toHave({
                err:null, prefix:'foo()', skip:true });


    expect().section('Typical usage');
    function myFunction(arg1, isProduction) {
        const v = new Validarg('myFunction()', isProduction);
        if (!v.boolean(arg1, 'arg1')) return v.err;
        return 'ok!';
    }
    expect(`myFunction(true)`,
            myFunction(true))
            .toBe('ok!');
    expect(`myFunction(123)`,
            myFunction(123))
            .toBe(`myFunction(): 'arg1' is type 'number' not 'boolean'`);
    expect(`myFunction(123, true) // tests skip`,
            myFunction(123, true)) // @TODO test that skip works with all methods
            .toBe('ok!');
}
